//
//  Copyright © 2016 Kairos. All rights reserved.
//

// Interface v1.4
/////////////////////

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

#define kAllowKNotification @"com_kairos_beaconManager_allowNotification"
#define kAllowKTracking @"com_kairos_beaconManager_allowTracking"

typedef void (^completion_block_t)(id __nullable error, id __nullable result);

@protocol KairosBeaconManagerDelegate <NSObject>

-(void) onError:(NSString * __nullable) message;

@end

@interface KairosBeaconManager : NSObject<CLLocationManagerDelegate>


/**
 *  When the beacon manager enters a region,
 * it actively scans for beacons during the scanDelay.
 * The default scan delay is 8 seconds and can be tweaked by setting directly this value.
 */
@property (assign, nonatomic) NSInteger scanDelay;

/**
 *  Sets the beacon manager delegate.
 */
@property (strong, nonatomic, nullable) id<KairosBeaconManagerDelegate> delegate;

/**
 * Get KairosBeaconManager singleton
 *
 * @return KairosbeaconManager singleton
 **/
+ (KairosBeaconManager * __nullable)sharedInstance:(NSString * __nonnull) secret
                                          appToken:(NSString * __nonnull) appToken
                                          delegate:(id<KairosBeaconManagerDelegate> __nonnull) delegate;


/**
 * When set to YES, shows KairosBeaconManager logs
 *
 * @param debug <#debug description#>
 */
-(void) setDebug:(BOOL) debug;

/**
 * Start beacon region monitoring
 */
-(void) startBeaconMonitoring;

/**
 * Force beacon ranging.
 * This can be use to detect more actively beacons but is a little bit more energy eating.
 */
-(void) startRanging;

/**
 * Method to call to trigger notification appearance
 *
 * @return BOOL Whether or not we received a KNotification
 */
-(BOOL) isKNotificationReceived: (UILocalNotification * __nullable)localNotification;

/**
 * When set to NO, tracking is disabled
 *
 * @param enable YES / NO
 */
-(void) setTrackingEnabled:(BOOL) value;

/**
 * Check wether tracking is enabled or not
 *
 * return BOOL
 */
-(BOOL) hasTrackingEnabled;

/**
 * Allow/Disable Kairos Beacon notifications
 *
 * @param enable YES / NO
 */
-(void) setNotificationEnabled:(BOOL) value;

/**
 * Check wether Kairos notifications are enabled
 *
 * return BOOL YES if Kairos notifications are enabled
 */
-(BOOL) hasNotificationEnabled;

/**
 * Set max tracking interval time to match app country regulation.
 * Default is 13 months (french regulation).
 * When set to 0, tracking interval is considered infinity.
 */
-(void) setMaxTrackingInterval:(NSTimeInterval) interval;

/**
 * This method is aimed at helping app editors and developpers
 * check wether the app should ask again for user tracking authorization.
 * If the user accept tracking again, refreshOptinTrackingDate must be called.
 * return BOOL
 */
-(BOOL) shouldAskTracking;

/**
 * Refresh next authorization date for user tracking
 */
-(void) refreshOptinTrackingDate;


@end
